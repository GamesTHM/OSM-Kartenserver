This project just contains the central documentation for the OSM Server to run and its configfiles. This repository also provides an [Installation Tutorial](wiki_debian_org_OSM_tileserver_jessie.pdf) for Debian Jesse or similar systems.
All files and programms without any standard installation path are located in the home directory (/home/osm/) of the user osm.
During the installation the import of the openstreetmap data to the PostgreSQL needs a pretty huge amount of RAM depending on the mapsize you want to import.

# Installation Tutorial
[Installation Tutorial](wiki_debian_org_OSM_tileserver_jessie.pdf)

# Important Locations
* /home/osm/openstreetmap-carto-2.29.1/style.xml  
Style definition for the renderd service how the map tiles have to look.
* /var/lib/mod_tile/default/  
Cache for the rendered map tiles. This directory needs be deleted if the style is changed, otherwise the server will serve the tiles in the old style.
* /etc/init.d/renderd  
Start/Stop script for the daemon. This needs to be restarted if the changes to the style.xml should be applied.

Required Software Packages:
* [PostgreSQL](README.md#postgresql)
* [mod_tile](README.md#mod_tile)
* [mapnik](README.md#mapnik)
* [renderd](README.md#renderd)

The following sections will mention files that could be needed to be edited in the current operations.

## PostgreSQL
The PostgreSQL installation is just a standard installation without any adaptions needed for this service to run. It just serves the mapdata to be rendered into images by renderd.

## mod_tile
The mod_tile module is needed for the Apache2 to provide the maptiles. It needs to be compiled to be installed but this step is explained in the [Installation Tutorial](wiki_debian_org_OSM_tileserver_jessie.pdf). When it is running, there shouldn't be any need to edit any config files. 

## mapnik
Primary this provides the shapefiles and stuff for the renderd. No edits needed in current operations.

## renderd
This is where the magic happens. Initially it needs to be told in the /usr/local/etc/renderd.conf where to find the style.xml, pluginsdirectory and the hostname. But later there should not be any need to edit this file.



